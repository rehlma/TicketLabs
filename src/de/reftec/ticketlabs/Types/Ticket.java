package de.reftec.ticketlabs.Types;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class Ticket {
	
	private SimpleBooleanProperty archived;
	private SimpleIntegerProperty eventId;
	private SimpleIntegerProperty id;
	private SimpleDoubleProperty value;
	private SimpleBooleanProperty isUsed;
	private SimpleStringProperty date;
	private SimpleStringProperty time;
	
	public Ticket() {
        this.archived = new SimpleBooleanProperty();
        this.eventId = new SimpleIntegerProperty();
        this.id = new SimpleIntegerProperty();
    	this.value = new SimpleDoubleProperty();
    	this.isUsed = new SimpleBooleanProperty();
    	this.date = new SimpleStringProperty();
    	this.time = new SimpleStringProperty();
	}
	
	public boolean getArchived() {
		return archived.get();
	}
	public void setArchived(boolean archived) {
		this.archived.set(archived);
	}	
	public int getEventId() {
		return eventId.get();
	}
	public void setEventId(int eventId) {
		this.eventId.set(eventId);
	}	
	public int getId() {
		return id.get();
	}
	public void setId(int id) {
		this.id.set(id);
	}
	public String getDate() {
		return this.date.get();
	}
	public String getTime() {
		return this.time.get();
	}
	public void setUsed(String used) {
		this.date.set(used.substring(0,10));
		this.time.set(used.substring(11, 16));
	}
	public long getValue() {
        return (long)value.get();
	}
	public void setValue(double value) {
		this.value.set(value);
	}		
	public boolean getIsUsed() {
		return isUsed.get();
	}
	public void setIsUsed(boolean isUsed) {
		this.isUsed.set(isUsed);
	}
	
	@Override
	public String toString() {
		return "ID: " + id.get() + "\tValue: " + value.get() + "\tEvent ID: " + eventId.get() + "\tUsed: " + date.get() + " " + time.get() + "\tisUsed: " + isUsed.get();
	}
}